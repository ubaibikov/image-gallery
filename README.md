## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```
### Json
```
Вы можете уставноить свои json-значания в файл  /src/data/ImageUrls.json
```
## About
```
Для более коректной работы сетки, я мог использовать библиотеки ...
однако как по мне задача заключаеться в демонстрации способностей делегирвать логику на компоненты
```
